require('dotenv').load();
const axios = require('axios');
const fastify = require('fastify')({
  logger: process.env.NODE_ENV !== 'production',
});
const { SQS } = require('aws-sdk');

const JIRA_BASE_URL = process.env.JIRA_BASE_URL || 'http://localhost:4553';
const JIRA_SEARCH_PATH = '/rest/api/2/search';

const QUEUE_URL = process.env.QUEUE_URL || 'http://localhost:9324';
const QUEUE = new SQS({
  endpoint: QUEUE_URL,
  region: 'us-east-1',
});

const GENERIC_ERROR = 'The API has encountered an issue.';

module.exports = () => {
  fastify.get(
    '/api/issue/sum',
    (request, reply) => {
      const { query, name } = request.query;
      if (!query || !name) {
        reply.code(400);
        return reply.send('Please provide a `query` and `name` parameter.');
      }
      return axios.get(`${JIRA_BASE_URL}${JIRA_SEARCH_PATH}?q=${query}`).then((jiraResponse) => {
        const pointsSum = jiraResponse.data.reduce(
          (accumulator, currentValue) => accumulator + currentValue.fields.storyPoints, 0,
        );
        const params = {
          MessageBody: JSON.stringify({ name, totalPoints: pointsSum }),
          QueueUrl: `${QUEUE_URL}/queue/cst-test-queue`,
        };
        QUEUE.sendMessage(params, (err, data) => {
          if (err) {
            fastify.log.error(err);
            reply.code(500);
            return reply.send(GENERIC_ERROR);
          }
          return reply.send({ ...data, ...{ name, totalPoints: pointsSum } });
        });
      }).catch((err) => {
        fastify.log.error(err);
        reply.code(500);
        return reply.send(GENERIC_ERROR);
      });
    },
  );
  return fastify;
};
