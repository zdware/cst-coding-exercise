# cst-coding-exercise Solution 
I enjoyed solving this problem! It's a nice mix of using a variety of modern technologies in an optimized development team (Docker, mocking APIs, etc). 

When approaching this problem, I first thought about solving it in Golang, but I quickly dismissed that option because I don't have much time to learn Golang + return the solution in a reasonable time. So I chose something familiar with myself, **NodeJS**. I considered Java, but I felt the time invested would of been higher. It's been awhile, and I don't have the familiarity wtih Spring Boot to move fast. Given the time, I'm confident I could implement a similar solution in Java.  

## Frameworks/Libraries

* Fastify (performance focused web framework)
* AWS-SDK (putting messages onto our local ElasticMQ)
* Axios (HTTP requests)
* ESlint (Linter, Airbnb Style)

## Liberties/Assumptions

* It's not explictly stated if the parameters for the API calls are required or not. I treated them as required and the server will 400 if both `query` and `name` are not provided. 
* If any errors occur from the JIRA API or SQS/ElasticMQ, we fail fast and with a generic error.
* The original `jira-api.ejs` file only had 1 mocked request supplied. I'm not sure if this was intended, but I modified it to randomly generate data, partially based upon the query supplied. If you query for `ZDW`, you will get a random length issue list with titles like `ZDW-1024`. 
* I was unable get the `mountebank` docker image supplied in the problem statement working. I installed it globally in my node environment and ran it that way. 
* It is unclear on what the web server is suppose to return to the user after putting a message on the SQS queue. It decided to return the SQS Message information, in addition to the story points sum of all issues returned by the JIRA API.

## Tests

* Three unit tests asserting that a 400 is thrown when the required parameters are not supplied.
* Two load tests, using the tool [Tsung](http://tsung.erlang-projects.org/). Each operates over 10 seconds, throwing requests at a different rates, 200/second and 400/second, respectively.

## Instructions
Uses node `10.15.1` (latest LTS). Use a node version manager or [download here](https://nodejs.org/en/). 

The application requires `JIRA_BASE_URL` and `QUEUE_URL` be set as environment variables. [dotenv](https://github.com/motdotla/dotenv) is used if you would like to use a `.env` file.

1. Run `npm install` in the current directory.
2. Run the docker service dependencies. 
  * If the mountebank one doesn't work for you, execute `npm install -g mountebank`. Then run `mb --configfile mountebank/jira-api.ejs --allowInjection`.
3. Run `node app.js`
4. The server is now ready to accept requests on port :8080

To run unit tests, simply run `npm run test`. There is no dependency on the docker services for these. 

### Load Testing Instructions
**This may take up some CPU/Bandwidth, depending on your machine!**

1. Install [Tsung](http://tsung.erlang-projects.org/).
2. Run `tsung -k -f tsung/10_seconds_2000_users.xml start`
3. When the load test is done, you will console will print `All slaves have stopped; keep controller and web dashboard alive.`. Navigate to `localhost:8091`.
4. Click on Reports or Graphs on the top right for how the test went. Click on Stop when you are finished!.

Substitute the file in step 2 with `10_seconds_4000_users.xml` for a heavier test.

## Feedback/Gotchas Encountered

* Unfortunately, the AWS SDK always wants credentials of some sort even if you're using it locally. I made a user in IAM with literally no privleges, and this bypassed the check. 
* Mountebank docker approach did not work, It did not seem to consume the config file correctly, not sure if this was an issue mounting the file to the Docker volume, or a Mountebank issue. Corrected by using Mountebank as a global node module. 
* Not sure if this was intentional, but the Mountebank configuration file seemed half done, only supplying 1 response/issue. If part of this exercise is to solve that problem, great! I would suggest adding that communication into the problem/exercise statement.