process.env.NODE_ENV = 'test';
const tap = require('tap');
const fastify = require('./server')();

tap.tearDown(() => fastify.close());

tap.test('GET /api/issue/sum without required parameters', (t) => {
  fastify.inject({
    method: 'GET',
    url: '/api/issue/sum',
  }).then((response) => {
    t.strictEqual(response.statusCode, 400, 'The status code is 400, Bad Request.');
    t.end();
  }).catch((err) => {
    t.error(err);
    t.end();
  });
});

tap.test('GET /api/issue/sum with query parameter, but not name parameter.', (t) => {
  t.tearDown(() => fastify.close());
  fastify.inject({
    method: 'GET',
    url: '/api/issue/sum?query=test',
  }).then((response) => {
    t.strictEqual(response.statusCode, 400, 'The status code is 400, Bad Request.');
    t.end();
  }).catch((err) => {
    t.error(err);
    t.end();
  });
});

tap.test('GET /api/issue/sum with name parameter, but not query parameter.', (t) => {
  t.tearDown(() => fastify.close());
  fastify.inject({
    method: 'GET',
    url: '/api/issue/sum?name=test',
  }).then((response) => {
    t.strictEqual(response.statusCode, 400, 'The status code is 400, Bad Request.');
    t.end();
  }).catch((err) => {
    t.error(err);
    t.end();
  });
});
